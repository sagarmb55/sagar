//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

class Employee
{
    var id:Int
    var name:String?
    var age:Int
    var address: String
    var mobile:Int
    var type:EmplyType     //0-permanent, 1-contract basis
    var domain:EmplyDomain
  
    

    
    init(id:Int,name:String,age:Int,address:String,mobile:Int,type:EmplyType,domain:EmplyDomain)
    {
    self.id = id
    self.name = name
    self.age = age
    self.address = address
    self.mobile = mobile
    self.type = type
    self.domain = domain
    
    }
    
}


enum EmplyType : String
{
    
    case permanent = "permanent"
    case contractbasis = "contractbasis"
    
}


enum EmplyDomain  : String
{
    case iOS = "iOS"
    case android = "android"
    case EAS = "EAS"
}



class Greytrip
{
    
    
    
    
    var emplist = [Employee] ()
    var lastEmpID = 0
    
    func addEmply(name1:String,age1:Int,address1:String,mobile1:Int,type1:EmplyType,domain1:EmplyDomain) -> Int
    {
        lastEmpID += 1
        let emp = Employee(id: lastEmpID, name: name1, age: age1, address: address1, mobile: mobile1, type: type1, domain: domain1)

        emplist.append(emp)
        return lastEmpID
    }
    
    
    func delEmply(id1: Int)
    {
        
        let correctId = id1 - 1
         emplist.removeAtIndex(correctId)
        
    }
   
    
    
    func delAll()
    {
    lastEmpID = 0
    emplist.removeAll()
    
    }
    
 
    
    
    
    func editEmply(id1:Int, name1:String,address1:String,age1:Int,mobile1:Int,type1:EmplyType,domain1:EmplyDomain)
    {
        let correctId = id1 - 1
        
        emplist[correctId].name = name1
        emplist[correctId].address = address1
        emplist[correctId].age = age1
        emplist[correctId].mobile = mobile1
        emplist[correctId].type = type1
        emplist[correctId].domain = domain1
        
    }
    
    
    
    
    
    func printEmply(id1:Int) -> Employee?
    {
        let correctedID = id1 - 1
        var fetchedEmp: Employee?
        if correctedID < emplist.count && correctedID >= 0
        {
            let emp = emplist[correctedID]
            fetchedEmp = emp
        }
        return fetchedEmp
    }
    
    
    

    func printemplyDomian(domain1:EmplyDomain)
    {
        
        var count = emplist.count - 1
        while count >= 0 {
            let employee = emplist[count]


            if employee.domain == domain1
            {
                                
                print(employee.name)
                print(employee.id)
        
            }
            
            count -= 1
        }
    }


    func printEmplyType(type1: EmplyType)
    {
    
    
        var count = emplist.count - 1
        while count >= 0 {
            let employee = emplist[count]
            
            
            if employee.type == EmplyType.permanent
            {
                
                print(employee.name)
                print(employee.id)
                
            }
            
            count -= 1
        }

    }



}




let myGreyTip = Greytrip()










